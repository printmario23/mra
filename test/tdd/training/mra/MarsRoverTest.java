package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MarsRoverTest {

	MarsRover mar;
	List<String> planetObstacles = new ArrayList<>();
	@Before
	public void setUp() throws MarsRoverException {	
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");

		mar = new MarsRover(10,10,planetObstacles);
	}
	
	@Test
	public void testShouldCreateMarsRover() throws MarsRoverException {	
		assertTrue(mar.marsRoverCreated());
	}
	
	@Test
	public void testPlanetContainsObstacleShouldReturnTrue() throws MarsRoverException {	
		assertTrue(mar.planetContainsObstacleAt(4,7));
	}
	
	@Test
	public void testPlanetContainsObstacleShouldReturnFalse() throws MarsRoverException {	
		assertFalse(mar.planetContainsObstacleAt(4,8));
	}
	
	@Test 
	public void testExecuteEmptyCommandShouldReturnLandingPosition() throws MarsRoverException {	
		assertEquals("(0,0,N)",mar.executeCommand(""));
	}
	
	@Test 
	public void testExecuteLeftRotationCommandShouldReturnZeroZeroWestAsStatus() throws MarsRoverException {	
		assertEquals("(0,0,W)",mar.executeCommand("l"));
	}
	
	@Test 
	public void testExecuteRightRotationCommandShouldReturnZeroZeroEastAsStatus() throws MarsRoverException {	
		assertEquals("(0,0,E)",mar.executeCommand("r"));
	}
	
	@Test 
	public void testExecuteForwardCommandShouldReturnZeroOneNorthAsStatus() throws MarsRoverException {	
		assertEquals("(0,1,N)",mar.executeCommand("f"));
	}
	@Test 
	public void testExecuteForwardCommandShouldReturnOneZeroEastAsStatus() throws MarsRoverException {	
		mar.executeCommand("r");
		assertEquals("(1,0,E)",mar.executeCommand("f"));
	}
	@Test 
	public void testExecuteBackwardCommandShouldReturnZeroTwoNorthAsStatus() throws MarsRoverException {	
		mar.executeCommand("f");
		mar.executeCommand("f");
		mar.executeCommand("f");
		assertEquals("(0,2,N)",mar.executeCommand("b"));
	}	
	@Test 
	public void testExecuteMultipleCommandShouldReturnTwoTwoEastAsStatus() throws MarsRoverException {	
		assertEquals("(2,2,E)",mar.executeCommand("ffrff"));
	}
	@Test 
	public void testExecuteBackwardCommandShouldReturnZeroNineNorthAsStatus() throws MarsRoverException {	
		assertEquals("(0,9,N)",mar.executeCommand("b"));
	}
	@Test 
	public void testExecuteBackwardCommandShouldReturnZeroeigthNorthAsStatus() throws MarsRoverException {	
		assertEquals("(0,8,N)",mar.executeCommand("bb"));
	}
	@Test 
	public void testExecuteBackwardCommandShouldReturnNineZeroWestAsStatus() throws MarsRoverException {	
		assertEquals("(9,0,W)",mar.executeCommand("lf"));
	}
}
