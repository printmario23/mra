package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {

	private int planetR = 0;
	private int planetC = 0;
	private int roverX = 0;
	private int roverY = 0;
	private int[][] grid;
	private List<String> planetObstacles = null;
	private String direction="";
	
	public boolean marsRoverCreated() {
		return (planetR!=0 && planetC!=0 && planetObstacles!=null && !direction.equals(""));
	}
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {	
		
		this.planetObstacles = planetObstacles;
		this.planetR = planetX;
		this.planetC = planetY;
		this.grid = new int[planetX][planetY];
		this.direction = "N";
		
		for(int i=0; i<planetX; i++) {
			for(int j=0; j<planetY; j++) {
				grid[i][j] = 0;
			}
		}
		
		for(int i=0; i<planetObstacles.size(); i++) {
			String coordinate = planetObstacles.get(i);
			String[] spaceSplit = coordinate.split(" ");  
			if(spaceSplit.length>1) {
				throw new MarsRoverException();
			}else {
				String[] commaSplit = coordinate.split(",");
				if(commaSplit.length!=2) {
					throw new MarsRoverException();
				}else {
					String obX = commaSplit[0].substring(1);
					String obY = commaSplit[1].substring(0,commaSplit[1].length()-1);
					grid[Integer.parseInt(obX)][Integer.parseInt(obY)] = 1; 
				}
			}
		}
		grid[planetR-1][0] = 2;		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {	
		return (grid[x][y]==1);
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	private void findRoverXY() {
		for(int i=0; i<planetR; i++) {
			for(int j=0; j<planetC; j++) {
				if(grid[i][j] == 2) {
					this.roverY = i;
					this.roverX = j;
					break;
				}
			}
		}
	}
	
	private void executeRotation(String rotation) {
		if(rotation.equals("l")) {
			if(direction.equals("N")) {
				direction = "W";
			}else if(direction.equals("W")) {
				direction = "S";
			}else if(direction.equals("S")) {
				direction = "E";
			}else if(direction.equals("E")) {
				direction = "N";
			}
		}else if(rotation.equals("r")) {
			if(direction.equals("N")) {
				direction = "E";
			}else if(direction.equals("E")) {
				direction = "S";
			}else if(direction.equals("S")) {
				direction = "W";
			}else if(direction.equals("W")) {
				direction = "N";
			}
		}
	}
	
	private void executeMovement(String movement) {
		if(movement.equals("f")) {
				grid[roverY][roverX] = 0;
			
				if(direction.equals("N")) {
					if(roverY == 0) {
						roverY = planetR-1;
					}else {
						grid[roverY-1][roverX] = 2;
					}
				}else if(direction.equals("E")) {
					if(roverX == planetC-1) {
						roverX = 0;
					}else {
						grid[roverY][roverX+1] = 2;
					}
				}else if(direction.equals("S")) {
					if(roverY == planetR-1) {
						roverY = 0;
					}else {
						grid[roverY+1][roverX] = 2;
					}
				}else if(direction.equals("W")) {
					if(roverX == 0) {
						roverX = planetC-1;
					}else {
						grid[roverY][roverX-1] = 2;
					}
				}
			}else if(movement.equals("b")) {
			
				grid[roverY][roverX] = 0;
			
				if(direction.equals("N")) {
					if(roverY == planetR-1) {
						roverY = 0;
					}else {
						grid[roverY+1][roverX] = 2;
					}
				}else if(direction.equals("E")) {
					if(roverX == 0) {
						roverX = planetC-1;
					}else {
						grid[roverY][roverX-1] = 2;
					}
					
				}else if(direction.equals("S")) {
					if(roverY == 0) {
						roverY = planetR-1;
					}else {
						grid[roverY-1][roverX] = 2;
					}
					
				}else if(direction.equals("W")) {
					if(roverX == planetC-1) {
						roverX = 0;
					}else {
						grid[roverY][roverX+1] = 2;
					}
				}
			}
	}
	
	
	private void executeSingleCommand(String command) {
		
		findRoverXY();
		
		if(command.equals("l") || command.equals("r")) {
			executeRotation(command);
		}
		else {
			executeMovement(command);
		}
	}

	
	public String executeCommand(String commandString) throws MarsRoverException {

		for(int i = 0; i < commandString.length(); i++) {
			executeSingleCommand(Character.toString(commandString.charAt(i)));
		}
		
		findRoverXY();
		return "("+roverX+","+(planetR-(roverY+1))+","+direction+")";
	}

}
